#include <TopLoop/Core/FileManager.h>
#include <TopLoop/Core/Job.h>
#include <TopLoop/Core/SampleMetaSvc.h>
#include <TopLoop/CLI11/CLI11.hpp>

#include <THqLoop/THqLoopAlg.h>

#include <memory>

int main(int argc, char* argv[]) {
  spdlog::set_pattern("%n   %^[%=9l]%$   %v");
  auto runnerlog = spdlog::stdout_color_mt("runTHqLoop");

  // set up variables defined by command line arguments
  CLI::App app("THqLoop");
  std::string inDir;
  std::string outFile{"output.root"};
  std::string inTree{"nominal"};
  bool enableParticleLevel{false};
  bool debug{false};
  bool disableProgBar{false};
  // set up the command line arguments
  app.add_option("-d,--in-dir", inDir, "Input SgTop ntuple dataset directory")->check(CLI::ExistingDirectory)->required();
  app.add_option("-t,--tree-name", inTree, "Tree name to process (default is 'nominal')");
  app.add_option("-o,--out-file", outFile, "Output file name");
  app.add_flag("--enable-plevel", enableParticleLevel, "Enable particle level and truth branches");
  app.add_flag("--debug", debug, "Set logging to debug");
  app.add_flag("--disable-progbar", disableProgBar, "Disable progress bar");
  // processes the command line arguments
  // see https://github.com/CLIUtils/CLI11 for more
  CLI11_PARSE(app, argc, argv);

  if (debug) {
    spdlog::set_level(spdlog::level::debug);
  }

  // make file manager and set necessary properties
  // - set the tree name to analyze (from command line)
  // - enable plevel if directed from command line
  // - point it to directory defined from command line
  auto fm = std::make_unique<TL::FileManager>();
  fm->setTreeName(inTree);
  if (enableParticleLevel) {
    fm->enableParticleLevel();
  }
  fm->feedDir(inDir);

  // make algorithm and set output file name
  auto alg = std::make_unique<thq::THqLoopAlg>();
  alg->setOutFileName(outFile);

  // setup the job
  // - if command line asked for particle level set the correct loop type
  TL::Job job;
  if (disableProgBar) {
    job.disableProgressBar();
  }
  if (fm->particleLevelEnabled()) {
    job.setLoopType(TL::LoopType::RecoWithParticle);
  }
  TL_CHECK(job.setFileManager(std::move(fm)));
  TL_CHECK(job.setAlgorithm(std::move(alg)));
  return job.run();
}
