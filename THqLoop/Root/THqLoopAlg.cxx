#include <THqLoop/THqLoopAlg.h>

thq::THqLoopAlg::THqLoopAlg() {
  // set up logger for the class
  m_logger = setupLogger("thq::THqLoopAlg");
}

thq::THqLoopAlg::~THqLoopAlg() {
  // drop logger when class is destroyed
  spdlog::drop("thq::THqLoopAlg");
}

TL::StatusCode thq::THqLoopAlg::init() {
  // we must always run the base init function from TopLoop's
  // Algorithm class (this sets up a lot of the functionality).
  TL_CHECK(TL::Algorithm::init());

  // lets get the total sum of weights for the sample we're analyzing,
  // we'll retrieve the WeightTool and call the function which will
  // give us this number. Same for the sample cross section
  if (isMC()) {  // only != 1 if were looking at MC
    m_totalSumWeights = weightTool().generatorSumWeights();
    m_sampleXsec = weightTool().sampleCrossSection();
    // this is the number we'll use to scale the weight to 1/fb (Xsec
    // is in /pb so we multiply by 1000)
    m_1ifbScale = 1000.0 * m_sampleXsec / m_totalSumWeights;
  }
  else {  // looking at data
    m_totalSumWeights = 1.0;
    m_sampleXsec = 1.0;
    m_1ifbScale = 1.0;
  }
  return TL::StatusCode::SUCCESS;
}

TL::StatusCode thq::THqLoopAlg::setupOutput() {
  // always call parent version of function
  TL_CHECK(TL::Algorithm::setupOutput());

  // lets setup our ntuple output
  std::unique_ptr<TFile> outfileptr(TFile::Open(m_outFileName.c_str(), "UPDATE"));
  m_outFile = std::move(outfileptr);

  // make the name of the output tree the name of the input tree
  // prepended with "THqLoop_"
  m_outTreeName = fmt::format("THqLoop_{}", fileManager()->treeName());
  // make the ROOT TTree
  m_outTree = new TTree(m_outTreeName.c_str(), m_outTreeName.c_str());
  m_outTree->SetDirectory(m_outFile.get());

  // now lets make some branches
  m_outTree->Branch("weight_nominal", &m_weight_nominal);
  m_outTree->Branch("pt_lep1", &m_pt_lep1);
  m_outTree->Branch("pt_lep2", &m_pt_lep2);
  m_outTree->Branch("njets", &m_njets);
  // if MC and nominal tree save pileup systematic weight
  if (isMC() && isNominal()) {
    m_outTree->Branch("weight_sys_pileup_UP", &m_weight_sys_pileup_UP);
    m_outTree->Branch("weight_sys_pileup_DOWN", &m_weight_sys_pileup_DOWN);
  }

  return TL::StatusCode::SUCCESS;
}

TL::StatusCode thq::THqLoopAlg::execute() {
  // always call parent version of function
  TL_CHECK(TL::Algorithm::execute());

  // on every event lets grab/calculate the information we need from
  // the branches. remember, the branches are just called as if they
  // are functions.

  // the main MC weight
  if (isMC()) {
    m_weight_nominal = weight_mc() * weight_pileup() * weight_jvt() * weight_leptonSF() *
                       weight_bTagSF_MV2c10_Continuous() * m_1ifbScale;
    if (isNominal()) {  // these variables are only available in nominal trees
      m_weight_sys_pileup_UP = weight_mc() * weight_pileup_UP() * weight_jvt() *
                               weight_leptonSF() * weight_bTagSF_MV2c10_Continuous() *
                               m_1ifbScale;
      m_weight_sys_pileup_DOWN = weight_mc() * weight_pileup_DOWN() * weight_jvt() *
                                 weight_leptonSF() * weight_bTagSF_MV2c10_Continuous() *
                                 m_1ifbScale;
    }
  }
  else {
    m_weight_nominal = 1.0;
  }

  // always start with zero jets
  m_njets = 0;
  // loop over jets and count the jets that are pt > 25 Gev and inside |eta| < 2.5
  for (std::size_t i = 0; i < jet_pt().size(); i++) {
    float j_pt = jet_pt().at(i);
    float j_eta = jet_eta().at(i);
    if (j_pt > 25 * GeV && std::fabs(j_eta) < 2.5) {
      m_njets++;
    }
  }

  // a place to store lepton pts
  std::vector<float> lepton_pts;
  // get electron pts
  for (std::size_t i = 0; i < el_pt().size(); i++) {
    lepton_pts.push_back(el_pt().at(i));
  }
  // get muon pts
  for (std::size_t i = 0; i < mu_pt().size(); i++) {
    lepton_pts.push_back(mu_pt().at(i));
  }

  // sort pts and save leading and subleading values to output branches
  std::sort(std::begin(lepton_pts), std::end(lepton_pts));
  m_pt_lep1 = lepton_pts.at(0) * toGeV;  // leading lepton pt
  m_pt_lep2 = lepton_pts.at(1) * toGeV;  // subleading lepton pt
  assert(m_pt_lep1 > m_pt_lep2);         // lep1 pt should always be larger than lep2 pt

  // always fill on every event
  m_outTree->Fill();

  return TL::StatusCode::SUCCESS;
}

TL::StatusCode thq::THqLoopAlg::finish() {
  // always call parent version of function
  TL_CHECK(TL::Algorithm::finish());

  logger()->info("Saving output to tree {} in file {}", m_outTreeName, m_outFileName);

  // write out and close the file
  m_outFile->Write(0, TObject::kOverwrite);
  m_outFile->Close();
  return TL::StatusCode::SUCCESS;
}
