# THqLoop

This is an ATLAS-cmake-style package that is meant to be compiled
alongside TopLoop in an ATLAS Analysis Release.

The three main files to use as a starting point for getting an
algorithm developed:

- `THqLoop/THqLoopAlg.h`: Algorithm class header
- `Root/THqLoopAlg.cxx`: Algorithm class implementation
- `util/runTHqLoop.cxx`: `main` to drive the algorithm via TopLoop's `FileManager` and `Job`

These files contain quite a few comments describing the basic
workflow.

For this example we setup the algorithm to save some very simple event properties:

- Save the nominal weight
- Save the pileup systematic weights (UP and DOWN)
- Save the leading and subleading lepton transverse momenta
- Save the total number of jets that have transverse momentum higher
  than 25 GeV and have absolute pseudorapidity inside 2.5.

We have a very mature package (called WtLoop) which uses TopLoop for
the tW analysis:
https://gitlab.cern.ch/atlasphys-top/singletop/tW_13TeV_Rel21/WtLoop/tree/master

I've also included `YamlCppInterface` as another submodule, if one
would like the yaml-cpp libraries at some point in the future (this is
something that WtLoop does).
