## CMakeLists.txt for THqLoop package to be built within ATLAS
## Analysis Relase 21.2.X series.
## Author: Doug Davis <ddavis@cern.ch>

# Declare the name of this package:
atlas_subdir( THqLoop None )

# This package depends on other packages:
atlas_depends_on_subdirs( PUBLIC
  TopLoop
  )


# This package uses ROOT:
find_package( ROOT REQUIRED COMPONENTS Physics Core Tree Hist RIO MathCore TreePlayer )

# Build a library that other components can link against:
atlas_add_library( THqLoop
  THqLoop/*.h Root/*.cxx Root/*.h
  PUBLIC_HEADERS THqLoop
  SHARED
  LINK_LIBRARIES TopLoop
  PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} YamlCppInterfaceLib
  PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}  YamlCppInterfaceLib)

target_include_directories( THqLoop PRIVATE ${CMAKE_BINARY_DIR}/generated )

add_dependencies( THqLoop yaml-cpp )

## Only necessary if you want to build ROOT dictionaries for the C++
## code (e.g. for use in PyROOT)
## Not used by default
#atlas_add_dictionary( THqLoopDict
#  THqLoop/THqLoopDict.h THqLoop/selection.xml
#  LINK_LIBRARIES THqLoop )

# Install data files from the package:
atlas_install_data( data/* )

# Install python modules
atlas_install_python_modules( python/* )

# Install scripts
atlas_install_scripts( scripts/* )

atlas_add_executable( runTHqLoop
  util/runTHqLoop.cxx
  LINK_LIBRARIES ${ROOT_LIBRARIES} THqLoop TopLoop )
