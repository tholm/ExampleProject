# TopLoop ExampleProject

This is an example ATLAS-cmake-style project built to analyze SgTop
ntuples with `TopLoop`. The `TopLoop` project is included as a [git
submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules). The
TopLoop documentation will be very helpful to understand what's going
here.

The main piece of code inside this git repository is the `THqLoop`
ATLAS-style package (this example was made for the tHq analyzers).

## Building

To build the project:

```sh
$ cd /path/to/work/area
$ mkdir build run
$ git clone https://gitlab.cern.ch/TopLoop/ExampleProject --recurse-submodules
$ cd build
$ asetup AnalysisTop,21.2,latest,slc6 ## or use a numbered release
$ cmake ../ExampleProject
$ source <target>/setup.sh # <target> will usually be of the form x86_64-slc7-gcc62-opt or similar
```

## Processing a SgTop ntuple dataset

The `THqLoop` project includes an executable called `runTHqLoop` which
runs the `THqLoopAlg` class (the algorithm class which inherits from
`TopLoop`'s `Algorithm` class.

See the list of options by asking for the help output:

```
$ runTHqLoop -h
```

Let's say you have the following SgTop ntuple somewhere on your local
disk:

```
/path/to/user.ddavis.mc16_13TeV.410648.PhPy8_Wt_DR_2l_t.SGTOP1.e6615_a875_r10201_p3554.ll.v28.2_out.root
```

We can process the nominal tree like so:

```
$ runTHqLoop -t nominal -o test_nominal.root \
    -d /path/to/user.ddavis.mc16_13TeV.410648.PhPy8_Wt_DR_2l_t.SGTOP1.e6615_a875_r10201_p3554.ll.v28.2_out.root
```

A systematic tree can be processed as well:

```
$ runTHqLoop -t EG_SCALE_ALL__1up -o test_EG_SCALE_ALL__1up.root \
    -d /path/to/user.ddavis.mc16_13TeV.410648.PhPy8_Wt_DR_2l_t.SGTOP1.e6615_a875_r10201_p3554.ll.v28.2_out.root
```

## Some more details

For more details see the README inside the `THqLoop` directory.
